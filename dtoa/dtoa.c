//
// Gertboard D to A code - modified from examples to drive the galvonometers.
//
// SPI control code


#include "gb_common.h"
#include "gb_spi.h"


// Set GPIO pins to the right mode
// DEMO GPIO mapping:
//         Function            Mode
// GPIO0=  unused
// GPIO1=  unused
// GPIO4=  unused
// GPIO7=  SPI chip select B   Alt. 0
// GPIO8=  unused
// GPIO9=  SPI MISO            Alt. 0
// GPIO10= SPI MOSI            Alt. 0
// GPIO11= SPI CLK             Alt. 0
// GPIO14= unused
// GPIO15= unused
// GPIO17= unused
// GPIO18= unused
// GPIO21= unused
// GPIO22= unused
// GPIO23= unused
// GPIO24= unused
// GPIO25= unused
//

// For D to A we only need the SPI bus and SPI chip select B
void setup_gpio()
{
   INP_GPIO(7);  SET_GPIO_ALT(7,0);
   INP_GPIO(9);  SET_GPIO_ALT(9,0);
   INP_GPIO(10); SET_GPIO_ALT(10,0);
   INP_GPIO(11); SET_GPIO_ALT(11,0);
} // setup_gpio


//
//  Read ADC input 0 and show as horizontal bar
//
void main(void)
{ int d, dummy;
  int direction = 0;
  int value = 0;
  

  printf ("These are the connections for the digital to analogue test:\n");
  printf ("jumper connecting GP11 to SCLK\n");
  printf ("jumper connecting GP10 to MOSI\n");
  printf ("jumper connecting GP9 to MISO\n");
  printf ("jumper connecting GP7 to CSnB\n");
  printf ("Map the I/O sections\n");
  setup_io();

  printf ("activate SPI bus pins\n");
  setup_gpio();

  printf ("Setup SPI bus\n");
  setup_spi();

  // Most likely, the DAC you have installed is an 8 bit one, not 12 bit so 
  // it will ignore that last nibble (4 bits) we send down the SPI interface.
  // So the number that we pass to write_dac will need to be the number
  // want to set (between 0 and 255) multiplied by 16. In hexidecimal,
  // we just put an extra 0 after the number we want to set.
  // So if we want to set the DAC to 64, this is 0x40, so we send 0x400
  // to write_dac.

  // To calculate the voltage we get out, we use this formula from the
  // datasheet: V_out = (d / 256) * 2.048

  printf ("Drawing square.\n");
  while(1) {
    
      for(value=0;value<254;value++) {
        d = value;
        write_dac(0, d*10);
        write_dac(1, 0);
      };
      for(value=0;value<254;value++) {
        d = value;
        write_dac(1, (d)*10);
        write_dac(0, 0);
      };
      for(value=0;value<254;value++) {
        d = value;
        write_dac(0, (256-d)*10);
	write_dac(1, 256*10);
      };
      for(value=0;value<254;value++) {
        d = value;
        write_dac(1, (256-d)*10);
        write_dac(0, 256*10);
      };
  
};

  
  
  restore_io();
} // main
